
//Dark and Light mode
let buttonMode = document.getElementsByTagName("p")[0];
let style = document.getElementsByTagName("link")[0].attributes.href;
//light mode
var mode = "light";

style.value = "/CSS/main.css";
// buttonMode.addEventListener("click", LightDark);

// function LightDark() {

// //to dark
// if ( mode ==="light" )
// {
//  	buttonMode.innerHTML = "☀️ Light Mode";
//  	mode = "dark";
// 	style.value = "/CSS/darkmode.css";
// }

// 	 //to light
// else if ( mode ==="dark" )
// {
// 	buttonMode.innerHTML = "🌑 Dark Mode";
// 	mode = "light";
// 	style.value = "/CSS/main.css";
// }

// }

//Display result section of page. Had to resuse old code from last year.

let quizSubmit = document.getElementsByTagName("button")[0];
quizSubmit.addEventListener("click", quizResult);
let resultdisplay = document.getElementById("displayresult");

function rightAnswer(n) {
	document.getElementsByClassName("questions")[n].style.borderRight=("solid 20px #009c00");
}

function wrongAnswer(n) {
	document.getElementsByClassName("questions")[n].style.borderRight=("solid 20px #c80815");
}

function quizResult() {
	let result = 0;

	if (document.getElementById("q1a2").checked){rightAnswer(0); result+=1; }
	else{wrongAnswer(0);}

	if (document.getElementById("q2a4").checked){rightAnswer(1); result+=1; }
	else{wrongAnswer(1);}

	if (document.getElementById("q3a1").checked){rightAnswer(2); result+=1; }
	else{wrongAnswer(2);}

	if (document.getElementById("q4a2").checked){rightAnswer(3); result+=1; }
	else{wrongAnswer(3);}

	if (document.getElementById("q5a1").checked){rightAnswer(4); result+=1; }
	else{wrongAnswer(4);}

	if (document.getElementById("q6a3").checked){rightAnswer(5); result+=1; }
	else{wrongAnswer(5);}

	if (document.getElementById("q7a3").checked){rightAnswer(6); result+=1; }
	else{wrongAnswer(6);}

	if (document.getElementById("q8a4").checked){rightAnswer(7); result+=1; }
	else{wrongAnswer(7);}

	if (document.getElementById("q9a2").checked){rightAnswer(8); result+=1; }
	else{wrongAnswer(8);}

	if (document.getElementById("q10a4").checked){rightAnswer(9); result+=1; }
	else{wrongAnswer(9);}


	result = result/10*100;

	resultdisplay.style.display = ("block");
	resultdisplay.children[0].innerText = `You got ${result}%.`;

	//Display result text and colour based on the obtained result
	if (result <= 25) {
		resultdisplay.children[1].innerText = " Do more research.";
		resultdisplay.style.borderColor = ("#c80815");
	}

	if (result > 25 && result <= 50) {
		resultdisplay.children[1].innerText = " There is always hope to learn.";
		resultdisplay.style.borderColor = ("#ffa500");
	}

	if (result > 50 && result <= 75) {
		resultdisplay.children[1].innerText = "Not bad. You may have missed some bits, but you are knowledgable.";
		resultdisplay.style.borderColor = ("#ffe500");
	}

	if (result > 75 && result < 100) {
		resultdisplay.children[1].innerText = "Nice. You have good knowledge.";
		resultdisplay.style.borderColor = ("#32ff1a");
	}

	if (result === 100) {
		resultdisplay.children[1].innerText = "Congrats! You're a walking encyclopedia!";
		resultdisplay.style.borderColor = ("#009c00");
	}

	location.href="quiz.html#header";

}
